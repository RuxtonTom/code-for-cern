app.service('voteService', function ($http, mainService, statsService) {
    //STILL WIP
    var votedOn;
    if (localStorage.getItem("votedOn") != null) {
        votedOn = JSON.parse(localStorage.getItem("votedOn"));
    }
    else {
        votedOn = [];
    }
    
    saveVotedOn = function () {
        localStorage.removeItem("votedOn");
        localStorage.setItem("votedOn", JSON.stringify(votedOn));
    }
    return {
        //Takes a post to vote, amount of votes to add and weather we're voting on a question or reply
        //Used to keep track of posts a user has voted on and does call to backend to inc/dec votes (backend does sanity checks as well)
        vote: function (post, amount, type) {
            var dir = ["u", "d"]; //char at start of array is direction we're voting. So here we're voting "u" (up)
            
            if (amount == -1)
                dir = ["d", "u"]; //voting down
            var id = type + dir[0] + post.id; //unique vote identifier
            
            if (votedOn.indexOf(id) == -1) {
                if (votedOn.indexOf(type + dir[1] + post.id) != -1) {
                    votedOn.splice(votedOn.indexOf(type + dir[1] + post.id));
                }
                else {
                    votedOn.push(id);
                }
            }
            
            
            post.votes = parseInt(post.votes) + amount; //Update vote count
            
            //Update post votes on backend
            $http.post(mainService.getPluginLink() + "vote=t", { id: parseInt(post.id), amount: amount }).then(function (res) {
            }, function (err) {
                console.error('ERR', err);
                //TODO: User err msg
                });

            
            saveVotedOn(); //keeps track of users votes when they've closed the app.
            
            statsService.trackEventD("Post voted", {"direction":dir[0]}); //Track usage stats
        },
        
        //Checks if the vote up/down button should be enabled
        //I.e, upvote should be disabled if a user has already voted up, but re-enable it if a the user decides to votes down.
        checkButtonEnabled: function (id) {
            if (votedOn.indexOf(id) == -1) {
                return false;
            }
            else {
                return true;
            }
        },
    }
});