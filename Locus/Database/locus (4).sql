-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 25, 2017 at 09:11 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `locus`
--

-- --------------------------------------------------------

--
-- Table structure for table `clues`
--

CREATE TABLE `clues` (
  `title` text NOT NULL,
  `text` text NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `huntid` text NOT NULL,
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `information` text NOT NULL,
  `hint1` text NOT NULL,
  `hint2` text NOT NULL,
  `hint3` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clues`
--

INSERT INTO `clues` (`title`, `text`, `lat`, `lng`, `huntid`, `id`, `question`, `answer`, `information`, `hint1`, `hint2`, `hint3`) VALUES
('', 'asdf', 54.97237295676004, -1.6246339947610977, 'PKFW50', 1, 'asdf', 'gvsd', 'ghf', '', '', ''),
('', 'dfbfx', 54.975722793263635, -1.6190550000101211, 'PKFW50', 2, 'sdffhs', 'hgff', 'dfgsdf', '', '', ''),
('', 'sad', 54.972801218208794, -1.6130744055226387, 'I6VK8Y', 4, 'asdf', 'gfd', 'sdf', '', '', ''),
('', 'asdf', 54.973047536607666, -1.6204129293873848, 'AXRYJS', 5, 'ds', 'fg', 'fgjfg', '', '', ''),
('', 'rdf', 54.977382493144034, -1.6188679769948067, 'AXRYJS', 6, 'dfg', 'dsd', 'fgs', '', '', ''),
('', 'te', 54.97421068883451, -1.6187255457043648, 'MIJQQE', 7, 'te', 'te', 'te', 'te1', 'n', 'n'),
('', 'te', 54.97421068883451, -1.6187255457043648, 'ICGBQO', 8, 'te', 'te', 'te', 'te1', 'n', 'n'),
('', 't2', 54.97578704515258, -1.616322286427021, 'Z9AFZL', 9, 't2t', 't2', 't2t', 't2', 't4', 't3'),
('', 'te', 54.97618112456327, -1.6220729425549507, 'MTWI7Y', 10, 'te', 'te', 'te', 'te', 'te', 'n'),
('', 'te2', 54.97760963000672, -1.6188113763928413, 'MTWI7Y', 11, 'te2', 'te2', 'te2', 'te2', 'te2', 'te2'),
('', 'te3', 54.978052948948935, -1.6118161752820015, 'MTWI7Y', 12, 'te3', 'te3', 'te3', 'te3', 'n', 'te3'),
('', 'sdc', 54.97440773675838, -1.6145198419690132, 'ZGGGNE', 13, 'q', 'j', 'd', 'djb', 'db', 'jldb'),
('', 'j', 54.9784962629965, -1.6087262704968452, 'ZGGGNE', 14, 'db', 'bd', 'kdb', 'kbd', 'kjbdk', 'idvfj');

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE `results` (
  `team` tinytext NOT NULL,
  `huntid` tinytext NOT NULL,
  `id` int(11) NOT NULL,
  `time` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `results`
--

INSERT INTO `results` (`team`, `huntid`, `id`, `time`) VALUES
('Team 1', '12', 1, '16:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clues`
--
ALTER TABLE `clues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clues`
--
ALTER TABLE `clues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `results`
--
ALTER TABLE `results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
