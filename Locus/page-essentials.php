<link rel="shortcut icon" type="image/png" href="Images/favicon.png"/>
<script src="JavaScript/jquery-3.2.1.min.js"></script>
<link rel="stylesheet" href="CSS/main.css?<?php echo date('l jS \of F Y h:i:s A'); ?>" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
<?php include_once "navbar.php";?>
<div id="topSpacer"></div>
<script src="JavaScript/smooth-scroll.js?<?php echo date('l jS \of F Y h:i:s A'); ?>"></script>
