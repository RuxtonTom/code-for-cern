<!DOCTYPE html>
<html>
    <!-- Created by Tom Ruxton -->
    <head>
        <title>Locus</title>
        <?php include_once "page-essentials.php";?>
        <script src="JavaScript/fade-in.js"></script>
    </head>
    
    <body id="homePage">
        <div class="fullBannerImage">
            <div class="bannerText">
                <h1>LOCUS</h1>
                <p>Are you ready for an adventure?</p>
                <div class="bannerButton"><a href="#get" class="smoothScroll">Get Locus</a></div>
                <div class="bannerButton" id="right" onclick="location.href='create-hunt.php'">Create Hunt</div>
            </div>
            <div id="overlay">
            </div>
        </div>
        <div class="fwSection">
            <div class="fwText">
                <h1>Features</h1>
                <table id="featureTable">
                    <tr>
                        <td><img src="Images/checkmark.png"></td>
                        <td><img src="Images/checkmark.png"></td>
                        <td><img src="Images/checkmark.png"></td>
                    </tr>
                    <tr>
                        <td><h1>Geo Location</h1></td>
                        <td><h1>Puzzle Solving</h1></td>
                        <td><h1>Hunt Making</h1></td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                With our Global Positioning Technology, Locus will be able to track where you are and will know when you've reached the location of the clue!
                            </p>
                        </td>
                        <td>
                            <p>
                                Solve clues under time pressure to the best of your ability. The quicker you are, the higher ranked you'll be and the more bragging rights you get!
                            </p>
                        </td>
                        <td>
                            <p>
                                Not satisfied with just doing hunts? Why not mastermind your own and put your friends to the test with our own Hunt Creator?
                            </p>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        
        <div class="fwSection" id="dark">
            <div class="fwText">
                <h1>Inspiration</h1>
                    <p>
                        We created Locus to enter the market of "Active Gaming". These days, traditional gaming is going from sitting still clicking a mouse and tapping on a keyboard to a more interactive experience. With Virtual Reality becoming increasingly popular and applications such as "Pokemon Go" being a huge success, proves gamers will happily be active for an impressive experience.
                        <br><br>
                        That's where Locus comes in. With the ability to create your own hunts and challenge your friends clue solving abilities under time pressure, or to take part in a hunt yourself and learn more about the place you're exploring. Whatever you do, you're guaranteed to have fun with Locus.
                    </p>
            </div>
        </div>
        
        <div class="fwSection">
            <div class="fwText">
                <h1>Some Title</h1>
                <p>
                    Need something not from the spec document, because that was shit.
                </p>
            </div>
        </div>
        
        <div class="fwSection" id="dark">
            <div class="fwText">
                <h1 id="get">Get Locus</h1>
                <p style="text-align: center;">
                    Enough talking about Locus, download it now!
                </p>
                
                <img src="Images/playstore.png" id="playStore">
            </div>
        </div>
        <?php include_once "footer.php";?>
    </body>
    
</html>