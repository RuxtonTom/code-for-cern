<?php
    //Created by Tom Ruxton
    //If you set up you Wamp correctly, these default values will work
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "locus";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    

//    include_once 'psl-config.php';   // As functions.php is not included
//    $mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
?>