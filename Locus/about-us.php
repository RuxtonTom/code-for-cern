<!DOCTYPE html>
<html>

	<head>
		<title>About Us</title>
        <?php include_once "page-essentials.php";?>
    </head>

	<body id="nohorizscroll">
		<div class="fullBannerImage" id="small">
			<div class="bannerText" id="small">
				<h1>About Us</h1>
			</div>
			<div id="overlay">
			</div>
		</div>
		<div id="intro">
			<p>We are team 7, developers of the Locus app.</p>
		</div>
			
		<div class="slidesContent container">
			
			<div class="container slideElement">
				<img src="images/James.jpg" style="width:100%">
				<div class="slidesDescription">
					James Atkinson
				</div>
			</div>
			
			<div class="container slideElement">
				<img src="images/Adam.jpg" style="width:100%">
				<div class="slidesDescription">
					Adam Gray
				</div>
			</div>
			
			<div class="container slideElement">
				<img src="images/Oana.jpg" style="width:100%">
				<div class="slidesDescription">
					Oana Ivanovici
				</div>
			</div>
			
			<div class="container slideElement">
				<img src="images/Electra.jpg" style="width:100%">
				<div class="slidesDescription">
					Electra Panagiotopoulou
				</div>
			</div>
			
			<div class="container slideElement">
				<img src="images/Tom.jpg" style="width:100%">
				<div class="slidesDescription">
					Tom Ruxton
				</div>
			</div>
			
			<div class="container slideElement">
				<img src="images/Paddy.jpg" style="width:100%">
				<div class="slidesDescription">
					Paddy Ryan
				</div>
			</div>
			
			<div class="container slideElement">
				<img src="images/Lucas.jpg" style="width:100%">
				<div class="slidesDescription">
					Lucas Vidalis
				</div>
			</div>
			
			<div class="container slideElement">
				<img src="images/Ben.jpg" style="width:100%">
				<div class="slidesDescription">
					Ben Wooding
				</div>
			</div>
		
			<button class="leftButton" onclick="plusDivs(-1)">&#10094;</button>
			<button class="rightButton" onclick="plusDivs(1)">&#10095;</button>
		</div>	

		</br> 
		
        <div class="fwText">
            			
			<p>
                We are a group of Computer Science students at Newcastle University. On our first year at university, our coordinators had organized a treasure hunt for us to do within our tutor groups. For the hunt we were given a paper with the clues and we had to go from there, finding all the locations that the clues referred to, taking pictures of us at the said locations, and returning back at the starting point. Keeping that activity in mind we felt like clues on paper were not representative of the School of Computing Science and that it was a waste of paper and too much staff had to be involved to coordinate this hunt. Hence, we decided to create a mobile application and a corresponding website that would be more appropriate for this tradition. However, we didn’t want to limit our possible audience to first year Computer Science students, which is why we made it possible for users to create their own treasure hunt on the website to play, challenge their friends, and share their results at the end of the hunt on different social media platforms, such as Facebook. With this application we hope to help students discover the university campus in a fun and alternative way. 
            </p>
        </div>
		
		
		<script>
		var slideIndex = 1;
		showDivs(slideIndex);
	
		function plusDivs(n) {
			showDivs(slideIndex += n);
		}	

		function showDivs(n) {
			var i;
			var x = document.getElementsByClassName("slideElement");
			if (n > x.length) {slideIndex = 1}    
			if (n < 1) {slideIndex = x.length}
			for (i = 0; i < x.length; i++) {
				x[i].style.display = "none";  
			}	
			x[slideIndex-1].style.display = "block";  
		}
		</script>
	
	<!--Slideshow script adapted from https://www.w3schools.com/w3css/w3css_slideshow.asp-->
	
	</body>
	
	
</html>