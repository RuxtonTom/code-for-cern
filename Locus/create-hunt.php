<!DOCTYPE html>
<html>
    <!-- Created by Tom Ruxton -->
    <head>
        <?php include_once "page-essentials.php";?>
        <title>Create Hunt</title>
    </head>

    <body oncontextmenu="return false;">
        <div class="fullBannerImage" id="small">
            <div class="bannerText" id="small">
                <h1>Create Hunt</h1>
            </div>
            <div id="overlay">
            </div>
        </div>
        <div id=instr>
            <h2>Instructions</h2>
            <p>
                Welcome to the Locus Hunt Creator. Right click on the map to add a position at that location, then fill in the subsequent fields.
                <br><br>
                <b>Title</b> -> The name of the location, e.g. "Library".
                <br><br>
                <b>Clue</b> -> A short cryptic message on what the location is, e.g. a for a library the clue could be "A place to read books".
                <br><br>
                <b>Questions/Answer</b> -> To verify the user has reached the location, the question should be something specfic to the place. The answer is what the user must type in to progress onto the next clue. E.g. Question: "What colour is the reception desk", Answer: "Blue".
                <br><br>
                <b>Information</b> -> A small bit of interesting text about the location. E.g. "The library was created in...".
                <br><br>
                <b>Hints</b> -> Incase your clue is too clever to understand, you can add hints to make it easier for the user to figure out the location if they decide to sacrifice time.
                <br><br>
                You can right click on the last placed position to remove it.
            </p>
        </div>
        <div id="map"></div>
        <script>
            var clueCounter = 1;
            //Creates the google maps view, and centres it on newcastle
            function initMap() {
                var newcastle = {
                    lat: 54.97438490759,
                    lng: -1.615337454713881
                };
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 15,
                    center: newcastle
                });

                //store lat and lng of right click
                google.maps.event.addListener(map, "rightclick", function (event) {
                    var lat = event.latLng.lat();
                    var lng = event.latLng.lng();
                    var markerPos = {
                        lat: lat,
                        lng: lng
                    };
                    addMarker(markerPos);

                });

                //Adds the google maps marker at the specified positions
                function addMarker(newMarker) {
                    var marker = new google.maps.Marker({
                        position: newMarker,
                        map: map,
                        title: 'Position '+clueCounter
                    });
                    dynamicFields(true, newMarker.lat, newMarker.lng);
                    google.maps.event.addListener(marker, 'rightclick', function (event) {
                        var temp = clueCounter -1;
                        if(marker.title == "Position "+temp){
                            marker.setMap(null);
                            dynamicFields(false, 0, 0);
                        }
                    });
                }
            }

            //Dynamically adds/removes form fields based on google map markers
            function dynamicFields(add, lat, lng){
                var form = document.getElementById("dynamicForm");
                if(add){

                    if(clueCounter > 1){
                        var divider = document.createElement("div");
                        divider.classList.add("formDivider");
                        divider.innerHTML = "Clue "+clueCounter;
                        form.appendChild(divider);
                    }

                    var label11 = document.createElement("label");
                    label11.htmlFor = "title"+clueCounter;
                    label11.innerHTML = "Title";

                    var input11 = document.createElement("input");
                    input11.type = "text";
                    input11.name = "title"+clueCounter;
                    input11.required = true;
                    input11.placeholder = "";
                    input11.id = "title"+clueCounter;

                    var label = document.createElement("label");
                    label.htmlFor = "clue"+clueCounter;
                    label.innerHTML = "Clue";

                    var input = document.createElement("input");
                    input.type = "text";
                    input.name = "clue"+clueCounter;
                    input.required = true;
                    input.placeholder = "";
                    input.id = "clue"+clueCounter;

                    var label5 = document.createElement("label");
                    label5.htmlFor = "question"+clueCounter;
                    label5.innerHTML = "Question";

                    var input5 = document.createElement("input");
                    input5.type = "text";
                    input5.name = "question"+clueCounter;
                    input5.id = "question"+clueCounter;
                    input5.required = true;
                    input5.placeholder = "";

                    var label6 = document.createElement("label");
                    label6.htmlFor = "answer"+clueCounter;
                    label6.innerHTML = "Answer";

                    var input6 = document.createElement("input");
                    input6.type = "text";
                    input6.name = "answer"+clueCounter;
                    input6.required = true;
                    input6.placeholder = "";
                    input6.id = "answer"+clueCounter;

                    var label7 = document.createElement("label");
                    label7.htmlFor = "information"+clueCounter;
                    label7.innerHTML = "Information";

                    var input7 = document.createElement("input");
                    input7.type = "text";
                    input7.name = "information"+clueCounter;
                    input7.required = true;
                    input7.placeholder = "";
                    input7.id = "information"+clueCounter;

                    var label8 = document.createElement("label");
                    label8.htmlFor = "hint1"+clueCounter;
                    label8.innerHTML = "Hint 1";

                    var input8 = document.createElement("input");
                    input8.type = "text";
                    input8.name = "hint1"+clueCounter;
                    input8.required = true;
                    input8.placeholder = "";
                    input8.id = "hint1"+clueCounter;

                    var label9 = document.createElement("label");
                    label9.htmlFor = "hint2"+clueCounter;
                    label9.innerHTML = "Hint 2";

                    var input9 = document.createElement("input");
                    input9.type = "text";
                    input9.name = "hint2"+clueCounter;
                    input9.placeholder = "Not required";
                    input9.id = "hint2"+clueCounter;

                    var label10 = document.createElement("label");
                    label10.htmlFor = "hint3"+clueCounter;
                    label10.innerHTML = "Hint 3";

                    var input10 = document.createElement("input");
                    input10.type = "text";
                    input10.name = "hint3"+clueCounter;
                    input10.placeholder = "Not required";
                    input10.id = "hint3"+clueCounter;

                    var input2 = document.createElement("input");
                    input2.type = "hidden";
                    input2.name = "cluelat"+clueCounter;
                    input2.value = lat;

                    var input3 = document.createElement("input");
                    input3.type = "hidden";
                    input3.name = "cluelng"+clueCounter;
                    input3.value = lng;

                    form.appendChild(label11);
                    form.appendChild(input11);
                    form.appendChild(document.createElement("br"));

                    form.appendChild(label);
                    form.appendChild(input);
                    form.appendChild(document.createElement("br"));

                    form.appendChild(label5);
                    form.appendChild(input5);
                    form.appendChild(document.createElement("br"));

                    form.appendChild(label6);
                    form.appendChild(input6);
                    form.appendChild(document.createElement("br"));

                    form.appendChild(label7);
                    form.appendChild(input7);
                    form.appendChild(document.createElement("br"));

                    form.appendChild(label8);
                    form.appendChild(input8);
                    form.appendChild(document.createElement("br"));

                    form.appendChild(label9);
                    form.appendChild(input9);
                    form.appendChild(document.createElement("br"));

                    form.appendChild(label10);
                    form.appendChild(input10);

                    form.appendChild(input2);
                    form.appendChild(input3);


                    clueCounter++;
                }
                //removes the form inputs
                else{
                    var i = form.childNodes.length;
                    if(clueCounter < 1){
                        for(var j = i-1; j >= i-25; j--){
                            form.removeChild(form.childNodes[j]);
                        }
                    }
                    else{
                        for(var j = i-1; j >= i-26; j--){
                            form.removeChild(form.childNodes[j]);
                        }
                    }
                    clueCounter--;
                }
            }
        </script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCAVCKYYunVjVIyK9GH8Mdc6--i48LlmRw&callback=initMap">
        </script>

        <div id="formContainer">
            <h3>As you add more locations, relevant text boxes will appear here</h3>
            <form id="dynamicForm" action="add-hunt.php">
                <input type="submit" value="Submit the hunt" id="button">
                <br>
            </form>
        </div>
        <div style="clear:both;"></div>
    </body>

</html>