<?php
    //Created by Tom Ruxton
    include_once "db-connect.php";
    
    //This loop will generate a hunt id, make sure it's not in use, then continue.
    while(true){
        $length = 0;
        $generatedKey = "";
        $characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789"; // The alphabet it'll use
        while ($length <= 5) {
            $generatedKey .= $characters[mt_rand(0,strlen($characters)-1)]; 
            $length += 1;
        }
        echo $generatedKey;
        $sql = "SELECT * FROM clues WHERE huntid='$generatedKey'";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            //rerun the func
            echo "Key already exists, trying again...";
        }
        else{
            addToDB($generatedKey);
            break;
        }
    }

    //Sequentially adds all the clues to the db with the generated huntid
    function addToDB($huntID){
        $counter = 0;
        global $conn;
        while(true){
            $counter++;
            if(isset($_REQUEST["clue".$counter]) && isset($_REQUEST["cluelat".$counter]) && isset($_REQUEST["cluelng".$counter]) && isset($_REQUEST["question".$counter]) && isset($_REQUEST["answer".$counter]) && isset($_REQUEST["information".$counter]) && isset($_REQUEST["hint1".$counter]) && isset($_REQUEST["hint2".$counter]) && isset($_REQUEST["hint3".$counter]) && isset($_REQUEST["title".$counter])){
                
                //escapes all inputs
                foreach(array_keys($_REQUEST) as $key){
                    $escaped[$key] = mysqli_real_escape_string($conn, $_REQUEST[$key]);
                }
                
                $title = $escaped["title".$counter];
                $text = $escaped["clue".$counter];
                $lat = $escaped["cluelat".$counter];
                $lng =  $escaped["cluelng".$counter];
                $question = $escaped["question".$counter];
                $answer = $escaped["answer".$counter];
                $info = $escaped["information".$counter];
                $hint1 = $escaped["hint1".$counter];
                
                //if the optional hints have not been set, set them as n
                if(!empty($_REQUEST["hint2".$counter])){
                    $hint2 = $escaped["hint2".$counter];
                }
                else{
                    $hint2 = "n";
                }
                
                if(!empty($_REQUEST["hint3".$counter])){
                    $hint3 = $escaped["hint3".$counter];
                }
                else{
                    $hint3 = "n";
                }
                
                $sql = "INSERT INTO clues (title, text, lat, lng, huntid, question, answer, information, hint1, hint2, hint3)" .
                    "VALUES ('$title', '$text', '$lat', '$lng', '$huntID', '$question', '$answer', '$info', '$hint1', '$hint2', '$hint3')";
                
                if(!$conn->query($sql)){
                    echo "<br>".$conn->error;
                }
                
            }
            else if($counter > 1000){
                echo "Either an error occured or you tried to add too many clues. Delete some clues and try again.";
                break;
            }

            else{
                $counter -=1;
                echo "<br>Only ".$counter." clues set";
                header("Location:hunt-added.php?huntID=".$huntID);
                break;
            }
        }
    }
?>