<?php
    //Created by Tom Ruxton
    include_once "db-connect.php";
    if(isset($_REQUEST["huntID"]) && isset($_REQUEST["web"])){
        getResults(mysqli_real_escape_string($conn, $_REQUEST["huntID"]), mysqli_real_escape_string($conn, $_REQUEST["web"]));
    }
    else{
        echo "";
    }
    
    //returns a json_encode of all results if web set to false (i.e. request was from the app) or prints a stylised ordered list if web was true (from website, and thus was AJAX call from check-hunt)
    function getResults($huntID, $web){
        global $conn;
        $sql = "SELECT * FROM results WHERE huntid='$huntID' ORDER BY time ASC";
        $result = $conn->query($sql);
        $huntResults = array();
        while($team = mysqli_fetch_assoc($result)) {
            $huntResults[] = $team;
        }
        if($web == "false"){
            echo json_encode($huntResults);
            return;
        }
        else{
            if(count($huntResults) < 1){
                echo "<p id=message>There are no results for that Hunt ID</p>";
            }
            else{
                echo "<ol id=resultsList>";
                foreach($huntResults as $team){
                    $minutes = floor($team["time"] / 60);
                    $seconds = $team["time"] % 60; 
                    echo "<li>".$team["team"]." - ".sprintf("%02d", $minutes).":".sprintf("%02d", $seconds);
                }
                echo "</ol>";
            }
        }
    }
?> 