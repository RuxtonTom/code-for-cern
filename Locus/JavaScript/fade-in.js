//Created by Tom Ruxton
$(document).ready(function() {
    $(window).scroll( function(){
        //will fade the box in when scrolled to
        $('.fwText').each( function(i){
            var tolerence = 400
            var windowBottom = $(window).scrollTop() + $(window).height();
            var divBottom = $(this).offset().top + $(this).outerHeight();
            //fade in if it's in the window and tolerence met
            if( windowBottom + tolerence > divBottom ){
                $(this).animate({'opacity':'1', 'margin-top':'0px'},650);   
            }
            
        }); 
    
    });
    
    $('.bannerText').animate({'opacity':'1', 'margin-top':'35vh'},1200);
    
});