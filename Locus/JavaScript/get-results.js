//Created by Tom Ruxton
$(document).ready(function(){
    //AJAX Query which sends the hunt id to get-results, and gets the responce and displays to the user
    $("#resultsBox").html("<p id=message>Enter a Hunt ID above to see the results</p>");
    $("#checkHuntForm").submit(function(event){
        event.preventDefault();
        var $form = $(this);
        var $inputs = $form.find("input");
        var $formatted = $inputs.serialize();
        $inputs.prop("disabled", true);

        request = $.ajax({
            url: "get-results.php",
            type: "get",
            data: $formatted
        });

        request.done(function(response, textStatus, jqXHR){
            $("#resultsBox").html(response);
            $inputs.prop("disabled", false);
        });

        request.failed(function(error, textStatus, jqXHR){
            $("#resultsBox").html("<p id=message>Error:"+error+"<br>"+textStatus+"</p>");
        });
    });
});
