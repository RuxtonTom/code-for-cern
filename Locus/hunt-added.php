<html>
    <!-- Created by Tom Ruxton -->
    <head>
        <?php include_once "page-essentials.php";?>
        <title>Success! - <?php echo $_REQUEST["huntID"];?></title>
    </head>
    <body id="huntadded">
		<div class="fullBannerImage" id="small">
			<div class="bannerText" id="small">
				<h1>Hunt Added</h1>
			</div>
			<div id="overlay">
			</div>
		</div>
		<div class="text">
			<h1>Success! Your hunt has been added</h1>
		</div>
		<img class="img" src="Images/checkmark.png">
        <div class="text">
			<p>
				Simply put this key: <b><?php echo $_REQUEST["huntID"];?></b> on the app and you're ready to go!
			</p>
        </div>
		<div class="text">
			<p>
				<a href="index.php">Let me make another!</a>
			</p>
		</div>
    </body>
</html>