************************ CONVENTIONS ************************

Files
- Separate all words with hyphens, i.e. first-second-third.php.

PHP
- Indent once inside the <?php?> tags.
- Use camel case for variables and function naming.

CSS + HTML
- Use camel case for class and id names.
- When adding CSS for a new section, header the block of code like has been done (i.e. comment in a title). This is done to make things easier to find.

SQL (Just to Note)
 - Never pass a variable straight to an SQL statement. Make sure you escape it first (google to find out how).
 
Other
- When making a new file, always put this in it <?php include_once "page-essentials.php";?>. This will automatically include the navbar, jQuery and the CSS.
- If you make a new file that should be included on every page (i.e. a footer), make sure you add it into the page-essentials.php, and then it will automatically be put on all pages.



************************ FILES ************************

Home page
- I'd suggest a large banner image at the top.
- Followed by blocks of text, each with it's own title.
    - As mentioned before, use placeholder text until the actual text is finalised.
- I suggest a sub nav bar underneath the static nav bar.
    - The sub nav bar could hold until scrolled past, at which point it then becomes fixed and sticks to the stop of the screen.
    - This sub nav bar would have the paragraph titles as links, and when clicked would smooth scroll to that part of the page (Not hard to do, google it).
- For some extra flair, I'd suggest having these blocks of text fade in when scrolled to. Again, easy to do with jQuery, just google it.


About us page
- I'd suggest similar styling to that of the home page purely for consistency.
    - It is your job to liaise with who ever might be working on that, and for you both to finish within the deadline.
- Additionally similar functionality (i.e. the sticky sub nav bar and the fading in text). Discuss amongst yourselves how you could split up the work.

Hunt results page
- This page will have display the team name and time it took for that team to do the hunt.
- The user should enter a huntid in a text box, and then a list of the teams + times for that hunt will be displayed. 
- If the entered huntid doesn't exist, the user should be told.
- This is easy to do with SQL and some PHP, google it.
    - If you want to be funky, you can do it using AJAX calls so that the page doesn't have to refresh and overall is a lot smoother.
- Make sure you echo the db results sensibly though. I.e. put them in a div. Make it easy for someone to come along and do some CSS to make it look good.
    
Hunt results page (design)
- Details will be provided once functionality is implemented

Hunt added design
- This page simple displays some text and the users huntid.
- Style the page so that it looks good, do this however you want but try to stay consistent with the rest of the site.

Get hunt page
- Takes a GET request with the huntid
- Query the db for the huntid then echo all the clues (for the specified hunt) and their relevant data in JSON format


************************ OTHER ************************

That should be everything for now. If you don't understand something just shoot me a message and I'll try and clarify. Additionally if you have literally no idea how to do something, try and find out, but again just message me if you're really stuck.

Make sure you stick to the deadlines, and if you don't think you'll make it tell me beforehand. It could be me setting an unrealistic deadline, so we'll figure it out.

Make sure you regularly update the repo. 

Last but not least, try and write all the code yourself, but also we are allowed to use outside code just as long as you reference it and clearly mark it.

When we finish all the first set of deadlines, we'll meet up and discuss what we've done, refine the process, then go again for sprint 2.

Happy coding!