<!DOCTYPE html>
<html>
    <!-- Created by Tom Ruxton -->
    <head>
        <?php
            include_once "page-essentials.php";
            include_once "db-connect.php";
        ?>
        <title>Results</title>
        
        <script src="JavaScript/get-results.js"></script>
    </head>
    <body>
        <div class="fullBannerImage" id="small">
                <div class="bannerText" id="small">
                    <h1>Results</h1>
                </div>
                <div id="overlay"></div>
        </div>
        <form id="checkHuntForm">
            <input type="text" placeholder="Hunt Id" required name="huntID">
            <input type="hidden" name="web" value="true">
            <input type="submit" value="Check" id="button">
        </form>
        <div id="resultsBox">
            <ol id="resultsList">
            </ol>
        </div>
    </body>
</html>