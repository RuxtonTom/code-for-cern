<?php
    //Created by Tom Ruxton
    include_once "db-connect.php";
    
    //if it's a new hunt 
    if(isset($_REQUEST["huntID"]) && isset($_REQUEST["teamName"]))){
        if(!teamExists($_REQUEST["huntID"], $_REQUEST["teamName"]))
            echo json_encode(getHunt($_REQUEST["huntID"]));
        else
            echo "false";
    }
    
    else if(isset($_REQUEST["getHuntIDs"])){
        echo json_encode(getHuntIDs());
    }
    
    else{
        echo "Error";
    }

    //returns all clues with the specified hunt id
    function getHunt($huntID){
        global $conn;
        $sql = "SELECT * FROM clues WHERE huntid='$huntID'";
        $result = $conn->query($sql);
        $clues = array();
        while($clue = mysqli_fetch_assoc($result)) {
            $clues[] = $clue;
        }
        return $clues;
    }

    //returns all the hunt ids
    function getHuntIDs(){
        global $conn;
        $sql = "SELECT DISTINCT huntid FROM clues";
        $result = $conn->query($sql);
        $output = array();
        while($huntID = mysqli_fetch_assoc($result)) {
            $output[] = $huntID;
        }
        return $output;
    }

    //returns true if the team name exists for that hunt id, false if not
    function teamExists($huntID, $teamName){
        global $conn;
        $sql = "SELECT * FROM results WHERE team='$teamName'";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            return true;
        }
        else{
            return false;
        }
    }
?>